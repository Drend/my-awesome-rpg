# coding: utf-8s
import re
from hero_assets import *
from mobs_assets import *
from random import randrange

classe = raw_input(
    "Bienvenue dans le mode combat veuillez choisir votre classe vous avez le choix entre \"guerrier\" ,"
    "\"pyromancien\"" "et \"archer\" \n")


class module(object):

    def module_combat(self):

        enemy_health = gobelin["health"]
        while enemy_health > 0:

            attaque = raw_input("Vous avez décide d'attaquer et vous êtes un {} "
                                "voici les attaque qui s'offre à vous\n {}, {} , {}\n".format
                                (classe, heroes_assets[classe]["attaque1"], heroes_assets[classe]["attaque2"],
                                 heroes_assets[classe]["attaque3"]))

            # ----------------------------------------A T T A Q U E 1----------------------------------------
            if attaque == heroes_assets[classe]["attaque1"]:
                if heroes_assets[classe]["random_critique"] <= heroes_assets[classe]["chance_critique"]:
                    coup_critique = heroes_assets[classe]["attaque1_damage"] * heroes_assets[classe]["coup_critique"]
                    enemy_health = enemy_health - coup_critique
                    print ("Bien joué ! Vous avez fait un coup critique et avez infligé {} de dégats "
                           "et il reste {} de vie aux gobelin ").format(coup_critique, enemy_health)
                else:
                    enemy_health = enemy_health - heroes_assets[classe]["attaque1_damage"]
                    print "Vous avez infligé {} de dégats et il reste {} de vie aux gobelin".format\
                        (heroes_assets[classe]["attaque1_damage"], enemy_health)

            # ----------------------------------------A T T A Q U E 2----------------------------------------
            elif attaque == heroes_assets[classe]["attaque2"]:

                if heroes_assets[classe]["random_critique"] <= heroes_assets[classe]["chance_critique"]:
                    coup_critique = heroes_assets[classe]["attaque2_damage"] * heroes_assets[classe]["coup_critique"]
                    enemy_health = enemy_health - coup_critique
                    print ("Bien joué ! Vous avez fait un coup critique et avez infligé {} de dégats "
                           "et il reste {} de vie aux gobelin ").format(coup_critique, enemy_health)
                else:
                    enemy_health = enemy_health - heroes_assets[classe]["attaque2_damage"]
                    print "Vous avez infligé {} de dégats et il reste {} de vie aux gobelin".format \
                        (heroes_assets[classe]["attaque2_damage"], enemy_health)

            # ----------------------------------------A T T A Q U E 3----------------------------------------
            elif attaque == heroes_assets[classe]["attaque3"]:
                if heroes_assets[classe]["random_critique"] <= heroes_assets[classe]["chance_critique"]:
                    coup_critique = heroes_assets[classe]["attaque3_damage"] * heroes_assets[classe]["coup_critique"]
                    enemy_health = enemy_health - coup_critique
                    print ("Bien joué ! Vous avez fait un coup critique et avez infligé {} de dégats "
                           "et il reste {} de vie aux gobelin ").format(coup_critique, enemy_health)
                else:
                    enemy_health = enemy_health - heroes_assets[classe]["attaque3_damage"]
                    print "Vous avez infligé {} de dégats et il reste {} de vie aux gobelin".format\
                        (heroes_assets[classe]["attaque3_damage"], enemy_health)

            # ----------------------------------------A T T A Q U E - M O B (WIP)---------------------------------------
            heroes_health = heroes_assets[classe]["health"] - gobelin["attaque2_damage"]
            print "le gobelin vous à infigé {} de dégats il vous reste {} de vie".format(gobelin["attaque2_damage"], heroes_health)

            if heroes_health <= 0:
                print 'le gobelin vous à tué félicitations'
                exit(0)

        if enemy_health <= 0:
            print "bravo vous avez tué le gobelin félicitations"
            exit(0)

