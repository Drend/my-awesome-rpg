# coding: utf-8
from random import randrange

heroes_assets = {

    "guerrier": {
         "health": 50,
         "attaque1": "coup d'epee",
         "attaque2": "frappe sanglante",
         "attaque3": "rage de vaincre",
         "attaque1_damage": randrange(8, 14),
         "attaque2_damage": randrange(13, 15),
         "attaque3_damage": randrange(20, 27),
         "chance_critique": 40,
         "coup_critique": 2.5,
         "random_critique": randrange(1, 101),
        },
    "archer": {
         "health": 40,
         "attaque1": "tir de flèche",
         "attaque2": "flèche du vent",
         "attaque3": "double flèche",
         "attaque1_damage": randrange(8, 16),
         "attaque2_damage": randrange(11, 19),
         "attaque3_damage": randrange(19, 33),
         "chance_critique": 70,
         "coup_critique": 2.5,
         "random_critique": randrange(1, 101),

        },

    "pyromancien": {
         "health": 30,
         "attaque1": "boule de feu",
         "attaque2": "vent de feu",
         "attaque3": "oiseau de feu",
         "attaque1_damage": randrange(3, 17),
         "attaque2_damage": randrange(6, 24),
         "attaque3_damage": randrange(10, 40),
         "chance_critique": 60,
          "coup_critique": 2.5,
         "random_critique": randrange(1, 101),

        },

}
